;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; confs
(package-initialize)

(add-to-list 'default-frame-alist
             '(ns-transparent-titlebar . t))
(add-to-list 'default-frame-alist
             '(ns-appearance . dark))

(after! evil-snipe
  (evil-snipe-mode -1))

(after! org
  (setq org-startup-indented nil))

(setq tab-width 2
			indent-tabs-mode t)
(setq-default evil-shift-width tab-width)
(setq-default electric-indent-inhibit t)

(use-package frame
	:ensure nil
	:custom
	(initial-frame-alist '((fullscreen . maximized)))
	:config
	(blink-cursor-mode -1)
	(when (member "Source Code Pro" (font-family-list))
		(set-frame-font "Source Code Pro-14:weight=regular" t t)))


(setq
 select-enable-clipboard t
 web-mode-markup-indent-offset 2
 web-mode-code-indent-offset 2
 web-mode-css-indent-offset 2
 css-indent-offset 2
 js-indent-level 2
 typescript-indent-level 2
 json-reformat:indent-width 2
 org-agenda-skip-scheduled-if-done t
 prettier-js-args '("--single-quote")
 projectile-project-search-path '("/run/media/thantez/Projects/")
 magit-hub-features t
 doom-themes-enable-bold t
 doom-themes-enable-italic t
 load-prefer-newer t
 org-ellipsis " ▾ "
 org-tags-column -80
 org-agenda-files (ignore-errors (directory-files +org-dir t "\\.org$" t))
 org-log-done 'time
 org-refile-targets (quote ((nil :maxlevel . 1)))
 +org-capture-todo-file "tasks.org"
 org-super-agenda-groups '((:name "Today"
                                  :time-grid t
                                  :scheduled today)
                           (:name "Due today"
                                  :deadline today)
                           (:name "Important"
                                  :priority "A")
                           (:name "Overdue"
                                  :deadline past)
                           (:name "Due soon"
                                  :deadline future)
                           (:name "Big Outcomes"
                                  :tag "bo")))

(use-package centaur-tabs
	:demand
	:bind (("C-S-<tab>" . centaur-tabs-backward)
				 ("C-<tab>" . centaur-tabs-forward)
				 ("C-x p" . centaur-tabs-counsel-switch-group))
	:custom
	(centaur-tabs-set-bar 'under)
	(x-underline-at-descent-line t)
	(centaur-tabs-set-modified-marker t)
	(centaur-tabs-modified-marker " ● ")
	(centaur-tabs-cycle-scope 'tabs)
	(centaur-tabs-height 30)
	(centaur-tabs-set-icons t)
	(centaur-tabs-close-button " × ")
	:config
	(centaur-tabs-mode +1)
	(centaur-tabs-headline-match)
	(centaur-tabs-group-by-projectile-project)
	(when (member "Arial" (font-family-list))
		(centaur-tabs-change-fonts "Arial" 130)))

(use-package vterm-toggle
	:after evil
	:custom
	(vterm-toggle-fullscreen-p nil)
	:config
	(evil-set-initial-state 'vterm-mode 'emacs)
	(global-set-key (kbd "C-`") #'vterm-toggle)
	(global-set-key (kbd "s-j") #'vterm-toggle)
	(add-to-list 'display-buffer-alist
							 '("^v?term.*"
								 (display-buffer-reuse-window display-buffer-at-bottom)
								 (reusable-frames . visible)
								 (window-height . 0.5))))


;; fonts
(setq doom-font (font-spec :family "Fira Code" :size 20)
      doom-variable-pitch-font (font-spec :family "SauceCodePro Nerd Font Mono" :size 21))

;; theme
(setq doom-theme 'doom-molokai)
(doom-themes-visual-bell-config)
(setq doom-themes-treemacs-theme "doom-colors")
(doom-themes-treemacs-config)
(doom-themes-org-config)

;; org dir
(setq org-directory "~/org/")

;; lines
(setq nlinum-highlight-current-line t)
(setq display-line-numbers 'relative)
(setq display-line-numbers-type 'relative)
(setq display-line-numbers-current-absolute t)

;; other confs
(use-package doom-modeline
	:hook (after-init . doom-modeline-mode)
	:custom
	(inhibit-compacting-font-caches t)
	(doom-modeline-buffer-file-name-style 'relative-from-project)
	(doom-modeline-bar-width 1)
	(doom-modeline-modal-icon nil)
	(doom-modeline-height 15)
	(doom-modeline-env-python-executable "python3")
	:config
	(when (member "Menlo" (font-family-list))
		(set-face-attribute 'mode-line nil :height 110 :font "Menlo")
		(set-face-attribute 'mode-line-inactive nil :height 110 :font "Menlo")))

(use-package all-the-icons
	:custom
	(all-the-icons-scale-factor 1.0))

(use-package all-the-icons-ivy
	:hook (after-init . all-the-icons-ivy-setup)
	:custom
	(all-the-icons-ivy-buffer-commands '()))

(use-package all-the-icons-dired
	:hook (dired-mode . all-the-icons-dired-mode))

(use-package highlight-numbers
	:hook (prog-mode . highlight-numbers-mode))

(use-package highlight-escape-sequences
	:hook (prog-mode . hes-mode))

(use-package rainbow-delimiters
	:hook (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-identifiers
	:hook (prog-mode . rainbow-identifiers-mode))

(use-package emojify
	:ensure t)

(custom-set-variables
 '(wakatime-api-key "d3654b60-54ea-40ac-8ba6-53645c99d8b1")
 '(wakatime-cli-path "/usr/bin/wakatime")
 '(wakatime-python-bin nil))
(global-wakatime-mode)

(use-package lsp-mode
  :init
  (add-to-list 'exec-path "/usr/lib/elixir-ls"))

;; use general for key binding
(use-package general
 :ensure t)

(general-define-key
 :states '(normal visual)

 "k" 'evil-record-macro ;; replace of q
 "l" 'evil-delete ;; replace of d
 "j" 'evil-insert ;; replace of i

 ;; movement
 "q" 'move-beginning-of-line ;; replace of C-a
 "e" 'move-end-of-line
 "a" 'evil-backward-char ;; replace of h
 "w" 'evil-previous-line  ;; replace of k
 "s" 'evil-next-line ;; replace of j
 "d" 'evil-forward-char ;; replace of l

 ;; append
 "i" 'evil-append ;; replace of a
 "Q" 'evil-insert-line ;; replace of I
 "E" 'evil-append-line ;; replace of A

 ;; word and section movement
 "A" 'evil-backward-WORD-begin
 "W" 'evil-backward-section-begin
 "D" 'evil-forward-word-begin ;; replace of w
 "S" 'evil-forward-section-begin

 "C-a" 'evil-backward-word-end
 "C-d" 'evil-forward-WORD-end ;; replace of e

 ;; scroll
 "I" 'evil-scroll-up
 "K" 'evil-scroll-down ;; replace of C-d
 "L" 'evil-scroll-right
 "J" 'evil-scroll-left

 ;; xref
 "gb" 'xref-find-definitions
 "gp" 'xref-pop-marker-stack

 ;; undo-tree
 "U" 'undo-tree-redo
	)

(map!
 :leader

 ;; main
 "R" 'counsel-grep-or-swiper ;; find something
 "S" 'projectile-ripgrep ;; find in project
 "B" 'counsel-bookmark ;; bookmark
 "T" 'treemacs ;; tree
 "r" 'recentf-open-files ;; recent files
 "v" '+vterm/toggle

 ;; window
 :desc "window (horizonatl) split" "wh" #'evil-window-split ;; vsplit is defined before
 :desc "window/workspace delete" "wC" #'+workspace/close-window-or-workspace

 :desc "window up" "ww" #'evil-window-up
 :desc "window down" "ws" #'evil-window-down
 :desc "window right" "wd" #'evil-window-right
 :desc "window left" "wa" #'evil-window-left

 :desc "far window up" "wW" #'evil-window-move-very-top
 :desc "far window down" "wS" #'evil-window-move-very-bottom
 :desc "far window right" "wD" #'evil-window-move-far-right
 :desc "far window left" "wA" #'evil-window-move-far-left

 :desc "window next" "wn" #'evil-window-next ;; evil-window-new is dangling
 :desc "window prev" "wp" #'evil-window-prev ;; evil-window-mru is dangling

 )
