;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

(package! all-the-icons-ivy)
(package! vterm-toggle)
(package! centaur-tabs)
(package! highlight-numbers)
(package! highlight-escape-sequences)
(package! rainbow-delimiters)
(package! rainbow-identifiers)
(package! general)
(package! wakatime-mode)
(package! emojify)
